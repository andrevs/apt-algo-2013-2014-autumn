#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

std::vector<int> prefix_function(const std::string& str) {
	int len = str.length();
	std::vector<int> p(len);
	for(int i = 1; i < len; ++i) {
		int j = p[i - 1];
		while(j > 0 && str[j] != str[i])
			j = p[j - 1];
		p[i] = j + (str[j] == str[i]);
	}
	return p;
}

void print(const std::vector<int>& vec) {
	for(int i = 1, n = vec.size(); i < n; ++i)
		std::cout << vec[i] << ' ';
	std::cout << std::endl;
}

int main() {
	std::string str;
	while(std::cin >> str) {
		std::vector<int> p = prefix_function(str);
		print(p);
	}
	return 0;
}
